import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

// Importaçõees para funconamento do Firebase e da auteenticação
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';

// Configurações do FIREBASE 
import { config } from '../config';

// Configurando o Storage
import { IonicStorageModule } from '@ionic/storage';
import { PreferencesProvider } from '../providers/preferences';
import { PerfilPage } from '../pages/perfil/perfil';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AngularFireDatabaseModule } from 'angularfire2/database';


@NgModule({
  declarations: [
    // ... aqui os componentes que serão usados apenas ná página atual
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    PerfilPage,
    RegisterPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),// import do pacote IonicStorageModule

    //Corrigir
    //Configurações do Firebase 
    AngularFireModule.initializeApp(config),
    //Configuração do serviço dee autenticação do firebase 
    AngularFireAuthModule,
    AngularFireDatabaseModule


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    // ... importe componentes que irão ser compartilhados entre si
    MyApp,
    PerfilPage,
    HomePage,
    TabsPage,
    LoginPage, // Registrando a página de login
    RegisterPage
  ],
  providers: [
    // ... serviços especificos das libs
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    PreferencesProvider

  ]
})
export class AppModule {}
