import { Storage } from "@ionic/storage";

//pacote para transformar nossa classe em injetável
import { Injectable } from '@angular/core';

@Injectable()
export class PreferencesProvider {


  constructor(private storage: Storage) {
    
  }
  //setando uma seção e passando o tipo de usuário
  create(user) {
    this.storage.set('user', user);
  }

  //Vai guardar o user mas antes vai ver se no appmodule ta pronto
  get(): Promise<any> {
    return this.storage.ready()
      .then(() => {
          return this.storage.get('user')
      })
  }

  // Quando deslogar deve remover do storage
  //usar o preference para deslogar
  remove(): Promise<boolean> {
    return this.storage.remove('user')
    .then(() => {
      return true
    })

  }
    
}
