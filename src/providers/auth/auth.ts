import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';


//ÍNICIO
// imports que deverão ser adicionados
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { User } from '../../models/user';
// FIM 
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class AuthProvider {

  //Atributo usuário que será usado para cadastro e autenticação
  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth, public firebaseDatabase: AngularFireDatabase) {
    this.user = firebaseAuth.authState;
    
  }

  //Metodo de cadastro

  register(user: User) {

    console.log(user);

    return this.firebaseAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(() => {
        let uid = this.firebaseAuth.auth.currentUser.uid;
        this.firebaseDatabase.database.ref('users').child(uid).set({
        name: user.name,
        phone: user.phone,
        cpf: user.phone,

        })

      }) 
  }

  //Método de login
  login(user: User) {
    return this.firebaseAuth.auth.signInWithEmailAndPassword(user.email, user.password);
  }

  // Método de logout 
  logout() {
    return this.firebaseAuth.auth.signOut();
  }

}