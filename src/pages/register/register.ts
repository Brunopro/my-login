import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { User } from '../../models/user';
import { TabsPage } from '../tabs/tabs';
import { AuthProvider } from '../../providers/auth/auth';



@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public user = {} as User;

  constructor(public navCtrl: NavController, public navParams: NavParams,
     private alertCtrl: AlertController, public loadingCtrl: LoadingController,
      private auth: AuthProvider) {
  }

  alert(title, message) {
    let al = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['Fechar']
    });
    al.present();
  }

  async register(user: User) {
    //2º
    let load = this.presentLoadingDefault();
    load.present();
    //end
    //Valida se foi informado email e passaword
    if(user.name == "" || user.email == "" || user.phone == "" || user.cpf == "" || user.password == "")
    {
      //3º
      load.dismiss();
      //end
      this.alert('Erro', 'É necessário informar o email e senha');
    } else {
      try{
        // Chama o método para cadatrar usuário
        const result = await this.auth.register(user);
        if (result) {
          
          //Se ocorrer tudo bem redireciona usuário para a página tabs
          this.navCtrl.setRoot(TabsPage);
        }
        //4º
        load.dismiss();
        //end
      } catch (e) {
        //5º
        load.dismiss();
        //end
        this.alert('Erro ao cadastrar', e.message);
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  presentLoadingDefault(): Loading {
    let loading = this.loadingCtrl.create({
      content: 'Por favor espere... '
    });
    return loading;
  }

}
