import { Component } from '@angular/core';

// Nesse import adicionamos o alertController quee será usado para apreseentar a mensagem no noso aplicativo
import { NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';

//Importaçã do nosso modelo de usuário
import { User } from '../../models/user';

//Importação do nosso serviço de auteenticação
import { AuthProvider} from '../../providers/auth/auth';

//Importação da página que o usuário será redirecionado após o login 
import { TabsPage } from '../tabs/tabs';
import { PreferencesProvider } from '../../providers/preferences';
import { RegisterPage } from '../register/register';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  //Definindo o nosso atributo usuário do tipo User
  public user = {} as User;

  loading: Loading

  //aqui no construtor vamos adicionar o AuthProvider e o AlertController 

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private auth: AuthProvider, private alertCtrl: AlertController,
              public loadingCtrl: LoadingController, public preference: PreferencesProvider )
 {  }

//Método para exibir as nossas mensagens de erro.
alert(title, message) {
  let al = this.alertCtrl.create({
    title: title,
    subTitle: message,
    buttons: ['Fechar']
  });
  al.present();
}

//Método usado para login do usuário
//Recebe como parametro um tipo user e tenta fazer o login
async login(user: User) {
  this.loading = this.presentLoadingDefault();
  this.loading.present();
  //Valida se foi informado email e password
  if(user.email == "" || user.password == "")
  {
    this.alert('Erro', 'É necessário informar o email e senha');
  } else {
    try{
      //Chama o método para fazer login
      const result = await this.auth.login(user);
      if (result) {
        //chamar metodo do prefere para criar o user aqui
        this.preference.create(user);
        //fim
        this.loading.dismiss();
        //Se ocorrer tudo bem redireciona para a página tabs
        this.navCtrl.setRoot(TabsPage);
      }
    } catch (e) {
      this.loading.dismiss();
      this.alert('Erro ao logar', e.message);
    }
  }
}

async register(user: User) {
  //2º
  let load = this.presentLoadingDefault();
  load.present();
  //end
  //Valida se foi informado email e passaword
  if(user.email == "" || user.password == "")
  {
    //3º
    load.dismiss();
    //end
    this.alert('Erro', 'É necessário informar o email e senha');
  } else {
    try{
      // Chama o método para cadatrar usuário
      const result = await this.auth.register(user);
      if (result) {
        
        //Se ocorrer tudo bem redireciona usuário para a página tabs
        this.navCtrl.setRoot(TabsPage);
      }
      //4º
      load.dismiss();
      //end
    } catch (e) {
      //5º
      load.dismiss();
      //end
      this.alert('Erro ao cadastrar', e.message);
    }
  }
}

onPush() :void {
    this.navCtrl.push(RegisterPage);
}

ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
}

//1º 
presentLoadingDefault(): Loading {
  let loading = this.loadingCtrl.create({
     content: 'Please wait...'
   });
  
  return loading;
}
//end
}
